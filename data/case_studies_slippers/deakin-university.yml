title: Deakin University
file_name: deakin-university
canonical_path: /customers/deakin-university/
twitter_image: /images/blogimages/deakin-social-image-1800x945.png
cover_image: /images/blogimages/deakin-cube-at-night-banner.jpg
cover_title: Deakin University cuts toolchain sprawl with GitLab
cover_description: Technology is a cornerstone of Deakin’s commitment to
  collaboration, as well as the university’s aim to become Australia’s most
  progressive and responsive university.
customer_logo: /images/case_study_logos/deakin-logo.png
customer_industry: Higher education
customer_location: Melbourne, Victoria, Australia
customer_solution: GitLab for Campuses (GitLab Ultimate)
customer_employees: 4,500
customer_overview: Deakin University is driving improvements in collaboration
  and productivity with the ease of use and out-of-the-box security features of
  GitLab’s DevSecOps Platform.
customer_challenge: Deakin University is driving improvements in collaboration
  and productivity with the ease of use and out-of-the-box security features of
  GitLab’s DevSecOps Platform.
key_benefits: >-
  **Improved developer experience**: The GitLab platform is popular with
  Deakin’s developers, who say it’s easy to use and well documented — meaning
  they can achieve what they need to achieve more quickly than ever.


  **Toolchain consolidation**: By embracing GitLab’s DevSecOps Platform, Deakin has been able to replace as many as four separate tools for CI/CD, source control management, and version control, plus a variety of security point solutions and open source tools.


  **Improved code quality**: With GitLab, code review processes are automated and built into reusable pipelines, meaning developers can commit code and get immediate feedback from their peers.
customer_stats:
  - stat: 60%
    label: reduction in manual tasks
  - stat: 100%
    label: of code in major projects scanned for quality
customer_study_content:
  - subtitle: A leader in distance education through modernized software delivery
    content: Deakin University was established in Melbourne, Victoria, Australia, in
      1974, and was one of the first universities in the region to specialize in
      distance education. Deakin has several research institutes, including the
      Applied Artificial Intelligence Institute, the Institute for Frontier
      Materials, and the Institute for Intelligent Systems Research and
      Innovation. In addition to excellence in teaching and research, Deakin has
      a deep history of collaboration, forging partnerships with industry and
      government to solve problems and share ideas.
  - subtitle: Tool proliferation creates fragmented processes
    content: >-
      Software engineering at Deakin University sits with a dozen or more
      delivery teams who are responsible for application architecture, software
      development, testing, and software operations. Aaron Whitehand, Deakin’s
      director of digital enablement, began to think about how Deakin’s software
      engineering teams might be able to modernize their software development
      processes after engaging with an IT partner who used DevOps practices.


      Top of mind for Whitehand was the complex nature of the university’s existing software development toolchain. “We had duplicate tools in teams, which led to ad hoc software development processes,” says Whitehand. “Not just duplication of CI/CD tools, but duplication of source control management tools and package management tools as well. And also, a number of different open source SAST tools that different teams were using with no real consistency, no real ability for us to audit things easily.” Tools that Deakin was using at the time included Bamboo, Circle CI, and Jenkins for continuous integration and continuous delivery (CI/CD) and Subversion for version control and source control management (SCM).


      With various departments within the software engineering organization using so many different tools — often for the same purpose — teams were burdened with manual tasks and duplicated efforts. “We had some simple code review processes that required handoffs across teams and were really lengthy, time consuming, and manual,” says Whitehand. “People were doing manual, repetitive tasks day in, day out.”


      Ultimately, the university’s sprawling toolchain meant Whitehand’s team was unable to provide the rest of the university — researchers, faculty, and students — with the right tools for source control, CI/CD, package management, software supply chain security, and collaboration with external partners. Plus, paying for multiple point solutions to solve related challenges within the development lifecycle became untenable as the university tightened budgets. “We were really trying to rationalize our software to save on total cost of ownership — not only licensing, but the skills we needed to maintain across our staff as well,” says Whitehand. Deakin needed an end-to-end platform that would allow developers to get feedback on their code more quickly, automate security testing, and provide much-needed consistency across the organization.
  - subtitle: A single platform to empower the university community
    content: >-
      Deakin spun up proofs-of-concept with several tools before choosing
      GitLab. For Whitehand, one of the factors that stood out most during the
      evaluation was how well the GitLab platform was received by developers.
      “After we spun up the environments for the technical proof of concept, the
      developers came back to us and it was overwhelming, the feedback of how
      easy it was to use, how well it was documented, and they could just pick
      it up seamlessly and achieve what they needed to achieve really, really
      quickly,” he says.


      Another advantage was the fact that GitLab integrates well with the tools that Deakin is already using — meaning they don’t need to rip and replace everything at once. The university is able to integrate GitLab with Jira for Agile and DevOps teams and HashiCorp Vault for secrets management. And thanks to [GitLab’s compatibility with Red Hat OpenShift](https://docs.gitlab.com/ee/install/openshift_and_gitlab/), Whitehand’s team is able to deploy GitLab to OpenShift clusters.


      GitLab’s regular release cadence, with valuable new features coming out on the 22nd of every month, was another standout factor in the decision. “We get excited when we see the next rounds of releases coming through because they tackle the challenges we always have,” says Michael Heley, group manager of software engineering. “Security is the biggest challenge for us along with many other organizations, so to see the growth and the maturity of the security options that are coming with the licensing we’ve got is great.” With GitLab, Deakin was able to implement a wide range of security scanners and then put the results straight into developers’ hands. This has allowed the university to replace various security point solutions and open source tools, cutting costs and complexity while also reducing the number of tool-related skill sets that had to be managed across teams.


      According to Hayden McFadyen, manager of software engineering at Deakin, one of the biggest benefits of GitLab is that it’s an all-in-one solution, which makes it easier to implement and maintain. “We've been upgrading since V13 and every upgrade has been painless,” he says. “The big win was really consolidating the products we were using for source control (SVN and Bitbucket), CI (Bamboo and Jenkins), and CD (Jenkins and Buildkite).”


      GitLab also offered a special package tailored to Deakin’s unique needs as an educational institution. The [GitLab for Campuses](https://about.gitlab.com/solutions/education/campus/) solution, which provides qualifying educational institutions with unlimited seats for all use cases and deployment methods, allowed Deakin to drive growth in GitLab usage across the university without being constrained by per-seat pricing. With GitLab for Campuses, Deakin is empowering research staff, faculty, interns, and students with a single tool for DevOps, source control, CI/CD, package management, and software supply chain security.
  - subtitle: Improved collaboration and code quality
    content: >-
      Deakin now has over 300 unique GitLab users in their internal environment.
      Since introducing GitLab, the university has been able to dramatically
      decrease manual efforts across their entire software supply chain, meaning
      teams can get code into production much faster. “With GitLab, the majority
      of those manual tasks have been automated, and I think we’ve cut at least
      60% of that time down,” says Whitehand.


      One area where the software engineering organization has seen significant improvement is the code review process. Previously, teams had been following a number of different processes, including various manual static application security testing (SAST) tools, to check code. With GitLab, these processes are automated and built into reusable pipelines. “Now developers can commit code and get immediate feedback from their peers,” says Whitehand. “We didn’t have that in the past — developers would wait sometimes days to get feedback from their seniors or their team leads on their code.”


      Deakin’s enablement team has also been able to build standardized processes and reusable templates — such as custom merge request templates, templated build pipelines, and a security and compliance framework — that can be shared with the broader university community and citizen developers, driving innovation and collaboration both inside the university and with key partners. “We were trying to bring in a community of practice and help it thrive for quite some time, but we were never successful until we had this tool,” says Whitehand.


      Process improvements in Deakin’s software development lifecycle have translated into higher-quality software and better experiences for end users. Since adopting GitLab, mean time to resolution (MTTR) for support tickets has dropped significantly. What’s more, GitLab’s end-to-end DevSecOps platform is driving closer connections between Deakin’s development and security teams. “We work quite closely with our security team now to understand our release risk,” says Whitehand. “Before GitLab, when we got to the stage where we were deploying something to production, we didn’t always know exactly what was going to happen.” Now, the university’s developers have newfound confidence that their code is going to work when it hits production.


      In 2021, Deakin was awarded a [Gartner Eye on Innovation Award for Higher Education](https://dteach.deakin.edu.au/2022/02/deakin-wins-gartner-innovation-award-for-3d-app/) for its work on ideAR, an approach to enabling teachers and learning designers to create interactive and engaging 3D and augmented reality experiences. Deakin used GitLab to support the development of the ideAR project.


      Deakin sees strong potential in investing in GitLab — both for the future of the university and its students, staff, and faculty. As part of Deakin’s central IT intern program, over the past 12 months the university has granted GitLab access to around 20 student interns from different departments, allowing those students to build real-world experience with GitLab and setting them up for success after graduation. Looking to the future, Deakin plans to make GitLab the platform of choice for teaching DevOps and other topics within computer science across the university.


      “There’s been a significant shift in developer empowerment as a result of our uptake of GitLab,” says Whitehand. “There really hasn’t been another initiative that has sparked the developers like GitLab has.”
customer_study_quotes:
  - blockquote: One of the drivers for us to adopt GitLab was the number of
      different out-of-the-box security features that allowed us to replace
      other solutions and open source tools and therefore the skill sets that
      came along with them.
    attribution: Aaron Whitehand
    attribution_title: Director of Digital Enablement, Deakin University
