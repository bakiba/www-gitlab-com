---
layout: markdown_page
title: "Category Direction - Runner Core"
description: "This is the Product Direction Page for the Runner Core product category."
canonical_path: "/direction/verify/runner_core/"
---

## Navigation & Settings

|                       |                               |
| -                     | -                             |
| Stage                 | [Verify](/direction/verify/)  |
| Maturity              | Lovable |
| Content Last Reviewed | `2023-03-31`                  |

### Introduction and how you can help

Thanks for visiting this direction page on the Runner Core category at GitLab. This page belongs to the [Runner Group](https://about.gitlab.com/handbook/product/categories/#runner-group) within the Verify Stage and is maintained by [Darren Eastman](mailto:deastman@gitlab.com).

### Strategy and Themes

GitLab Runner Core, as the GitLab CI/CD engine, enables GitLab's land and expand strategy. As DevOps use expands in the market, the critical problems customers need to solve for this product category include installing, configuring, and scaling CI build agents (runners) on public cloud infrastructure and securely executing CI jobs on heterogeneous computing environments.

### 1 year plan

The FY24 product themes for Runner Core mapped to GitLab's FY24 product investment themes are as follows.

**World-class DevSecOps experience**:

- [Next Runner Token Architecture](https://docs.gitlab.com/ee/architecture/blueprints/runner_tokens/#next-gitlab-runner-token-architecture)
- [Next Runner Autoscaling](https://docs.gitlab.com/ee/architecture/blueprints/runner_scaling/#next-runner-auto-scaling-architecture)
    - [Autoscaling on AWS EC2 instances](https://gitlab.com/groups/gitlab-org/-/epics/8856)
    - [Autoscaling on Google Compute Engine](https://gitlab.com/groups/gitlab-org/-/epics/9358)
    - [Autoscaling on Azure Virtual Machines](https://gitlab.com/groups/gitlab-org/-/epics/9359)
- [Kubernetes Executor - FY24](https://gitlab.com/groups/gitlab-org/-/epics/8394)

**Advanced security and compliance**

- Software Supply Chain Security - [Native Signing of Build Artifacts](https://gitlab.com/groups/gitlab-org/-/epics/9212)

### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months (April to June 2023) we plan to deliver these key features:

**Next Runner Autoscaling:**

- [GitLab Runner Fleeting plugin for AWS EC2 instances - Alpha](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29219)
- [GitLab Runner GCP Fleeting Plugin -  Alpha](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29217)
- [GitLab Runner Fleeting plugin for Azure Virtual Machines - Alpha](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29410)

### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

While the main focus area is the work required to deliver the new Runner autoscaling solution on instances provided by the major public cloud providers, new feature additions are also in the works for runner autoscaling on Kubernetes. The primary feature under development is [support for passing a custom Kubernetes PodSpec](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29659) to the Runner Kubernetes executor.

### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- Kubernetes executor - [support for activeDeadlineSeconds for CI job pods](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29279)
- Kubernetes executor - [overwrite generated pod specifications ](https://docs.gitlab.com/runner/executors/kubernetes.html#overwrite-generated-pod-specifications-alpha)

### What is Not Planned Right Now

We are not actively working on the following features:

- [Runner Priority](https://gitlab.com/gitlab-org/gitlab/-/issues/14976) - This is a very popular feature proposal in the Runner core product backlog that is meant to address the need to route CI jobs to an available or less busy Runner.

- [Access control list for instance runners](https://gitlab.com/gitlab-org/gitlab/-/issues/378322). This feature solves security, compliance, and organizational policy requirement problems for self-managed customers. While we are not actively developing this feature, we have started an [architectural blueprint](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/113923) for a new feature to solve the problem - the runner admissions controller solution. The goal is to document a technical approach that can guide community contributions for delivering an MVC.

- [Sticky Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/17497) - For this feature, the problem to solve is that users need to improve CI job performance in scenarios where each job can generate intermediate build elements with hundreds of GBs in size. In the current GitLab CI model, a significant amount of pipeline execution time is due to the uploading and downloading of intermediate build elements between jobs in a pipeline. Given the current Runner executor implementation, i.e., we support several executor types out of the box (shell, docker, Kubernetes), changing the CI job execution paradigm in GitLab is a significant architectural change. One option on the table is restricting this feature to Runners using the shell executor. The Sticky Runners MVC feature is not in the FY24 roadmap due to higher priority features in the Runner core and the Verify stage.

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecasted near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities

When you run a continuous integration pipeline job or workflow, the code in that pipeline must execute on some computing platform to complete your software's building, testing, and deployment. Terms used to describe the software that handles the pipeline code execution include worker, agent, or runner.

So while the basic functionality of pipeline code execution is table stakes in the industry, the ability to efficiently build software on multiple compute platforms with low operational maintenance overhead are critical features for a best-in-class solution.

#### Roadmap

In FY24 the key focus area for achieving best-in-class is Next Runner Autoscaling.

#### Top [1/2/3] Competitive Solutions

| Solution | CI/CD Agent naming convention/brand |Self-Managed Option Availablity|Notes|
| ------ | ------ |------ |------ |
|GitHub Actions| Runners |Available|GitHub released self-hosted runners in late 2019. Since then, GitHub has continued to invest in features and capabilities. We also notice similar investment themes as GitHub continues to target market segments requiring a self-managed platform. For example, GitHub self-hosted runners have added support for Apple M1 silicon. GitHub is also investing in self-hosted runner auto-scaling with plans to add support for Kubernetes in CY23-Q1. The autoscaling roadmap issue also includes an interesting note about queue visibility and tooling to deploy runners from the UI, which is very much related to our vision for zero friction runners.|
| Harness.io   | Harness Delegate                          |Available| Harness currently provides the following types of Delegate: Kubernetes, Shell Script, AWS ECS, Helm, Docker. Though the Delegates perform a similar essential function as GitLab Runner, i.e., executes tasks provided by the Harness Manager, the Delegates' primary purpose is to deploy software to the target platform. In this regard, the value proposition of the [GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/) is a critical consideration when evaluating capabilities in GitLab for developer frictionless cloud-native deployment.

### Maturity Plan

- Runner core is at the "Lovable" maturity level (see our [definitions of maturity levels](/direction/maturity/)).
- As detailed in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/6090), we plan to review the maturity scorecard for runner core and complete new category maturity scorecards for the other product development categories, runner cloud, and fleet management.
